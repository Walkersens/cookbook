import peewee
from peewee import *

from database import db

#top level entity
#Each recipe has one to many directions and ingredients
class Recipe(Model):
	name = TextField()
	url = TextField()
	timeEstimate = TextField()
	class Meta:
		database = db 

#How to make the recipe
class Direction(Model):
	recipeID = ForeignKeyField(Recipe, related_name='directions')
	directionDescription = TextField()
	class Meta:
		database = db 					

#the things necessary to make the recipe
class Ingredient(Model):
	recipeID = ForeignKeyField(Recipe, related_name='ingredients')
	name = TextField()
	#amount = CharField()
	class Meta:
		database = db 	
