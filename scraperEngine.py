import xml.etree.ElementTree as ET
import xml.etree as etree
from lxml import html
import requests
import requests_cache
parser = ET.XMLParser(encoding="utf-8")
tree = ET.parse('budgetBytesConfig.xml', parser=parser)

from Models import Recipe, Direction, Ingredient

#grabs a resource
def getWebResource(url):
	page = requests.get(url)
	return html.fromstring(page.content)

#parses the page for the information we want and stores it in the db
def parsePage(articleConfig, pageURL):
	page = getWebResource(pageURL)
	recipeNameConfig = articleConfig.find('recipeName')
	cookTimeConfig = articleConfig.find('cookTime')
	instructionsConfig = articleConfig.find('instructions')
	ingredientsConfig = articleConfig.find('ingredients')
	name = page.xpath(recipeNameConfig.get('path'))
	#this gets rid of the articles that are not recipes
	#however this isn't a generic solution if the engine will be used
	#for other websites
	if len(name):
		name = name[0].text
		cookTime = page.xpath(cookTimeConfig.get('path'))
		if(len(cookTime)):
			cookTime = cookTime[0].text
			rep = Recipe.get_or_create(name = name, url=pageURL, timeEstimate = cookTime)
			rep = Recipe.get(name = name, url=pageURL, timeEstimate = cookTime)
			instructions = page.xpath(instructionsConfig.get('path'))
						
			for instruction in instructions:
				if instruction.text:
					Direction.get_or_create(recipeID = rep, directionDescription = instruction.text)
			ingredients = page.xpath(ingredientsConfig.get('path'))
			for oneIngredient in ingredients:
				if oneIngredient.text:
					Ingredient.get_or_create(recipeID = rep, name=oneIngredient.text)
		
		


def main():
	paginator = ['blank']
	requests_cache.install_cache('demo_cache')
	#get the root of the config
	root = tree.getroot()
	#select the website root
	website = root.find('website')

	#now get the url address of the website	
	url = website.find("url") 
	url = url.get('href')
	#read the article config from the xml file
	articleConfig = website.find("article")
	paginatorConfig = website.find("paginator")
	#paginator = webResource.xpath(paginatorConfig.get("path"))
	print "Getting first page..."
	while(len(paginator)):
		print "Currently working on " + url
		#and pull the resource from the web
		webResource = getWebResource(url)
		#get all the articles from the page we just pulled
		articles = webResource.xpath(articleConfig.get("path"))
		#loop over all the matching elements
		for article in articles:
			parsePage(articleConfig, article.get('href'))

		#get all the articles from the page we just pulled
		#articles = webResource.xpath(articleConfig.get("path"))
		paginator = webResource.xpath(paginatorConfig.get("path"))
		url = paginator[0].get('href')
		

#head of the script
if __name__ == "__main__":
    main()
	


