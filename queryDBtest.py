import peewee
from peewee import *
from collections import OrderedDict
from database import db
from Models import Recipe, Direction, Ingredient
import sys
import unicodedata


def view_all():
	"""View all recipe id and recipe names in DB"""
	recipes = Recipe.select()
	i = 0
	for recipe in recipes:
		if i == 10:
			print('n) next page')
			print('q) return to main menu')
			next_action = raw_input('Action: ').lower().strip()
			if next_action == 'q':
				print('\n')
				break
			else:
				i = 0
		sys.stdout.write(str(recipe.id))
		sys.stdout.write(': ')
		sys.stdout.write(recipe.name)
		sys.stdout.write('\n\n')
		i = i + 1

		
def search():
	"""search recipes for name"""
	user_search = raw_input('Please enter your search: ')
	recipes = Recipe.select().where(Recipe.name.contains(user_search))
	i = 0
	for recipe in recipes:
		if i == 10:
			print('n) next page')
			print('q) return to main menu')
			next_action = raw_input('Action: ').lower().strip()
			if next_action == 'q':
				print('\n')
				break
			else:
				i = 0
		sys.stdout.write(str(recipe.id))
		sys.stdout.write(': ')
		sys.stdout.write(recipe.name)
		sys.stdout.write('\n\n')
		i = i + 1
	

def view_recipe_id():
	"""View specific recipe id"""
	id_usr = raw_input("\nPlease enter the numerical ID for a Recipe: ")
	the_recipe = Recipe.get(Recipe.id == id_usr)
	ingredients = Ingredient.select().where(Ingredient.recipeID == id_usr)
	directions = Direction.select().where(Direction.recipeID == id_usr)
	print('\n')
	sys.stdout.write(str(the_recipe.id))
	sys.stdout.write(': ')
	sys.stdout.write(the_recipe.name)
	sys.stdout.write('\n\n')
	print('Ingredients you will need:')
	for ingredient in ingredients:
		sys.stdout.write('     -')
		sys.stdout.write(ingredient.name)
		sys.stdout.write('\n')
	sys.stdout.write('\n')
	i = 1
	print('Directions for making recipe:')
	for direction in directions:
		sys.stdout.write('     ')
		sys.stdout.write(str(i))
		sys.stdout.write(') ')
		sys.stdout.write(direction.directionDescription)
		sys.stdout.write('\n\n')
		i = i + 1

def menu_loop():
	"""Show the menu"""
	choice = None
	while choice != 'q':
		print("Enter 'q' to Quit.")
		for key, value in menu.items():
			print('{}) {}'.format(key, value.__doc__))
		choice = raw_input('Action: ').lower().strip()
		if choice in menu:
			menu[choice]()
	

menu =  OrderedDict([
	('v', view_all),
	('s', search),
	('i', view_recipe_id),
])

if __name__ == '__main__':
	db.connect()
	menu_loop()
	



