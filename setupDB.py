import peewee
from peewee import *

#import the models
from Models import Recipe, Direction, Ingredient
#import the database handler
from database import db
#connect to the database
db.connect()
#create the tables
db.create_tables([Recipe, Direction, Ingredient])